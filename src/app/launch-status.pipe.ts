import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'launchStatus'
})
export class LaunchStatusPipe implements PipeTransform {

  transform(launch: any): string {
    if (launch.upcoming) {
      return 'Upcoming';
    }

    return launch.launch_success ? 'Success' : 'Fail';
  }

}
