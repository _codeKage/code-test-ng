import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SpinnerComponent } from './spinner/spinner.component';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { LaunchStatusPipe } from './launch-status.pipe';
import { DateToWordsPipe } from './date-to-words.pipe';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    LaunchStatusPipe,
    DateToWordsPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AccordionModule.forRoot(),
    InfiniteScrollModule
  ],
  providers: [ HttpClientModule ],
  bootstrap: [AppComponent]
})
export class AppModule { }
