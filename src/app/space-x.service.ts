import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SpaceXService {

  constructor(private httpClient: HttpClient) { }

  getResults(limit: number = 9999, offset: number = 0): Observable<any[]> {
    const params = new HttpParams();
    params.append('offset', offset.toString());
    params.append('limit', limit.toString());
    return this.httpClient.get<any[]>(`https://api.spacexdata.com/v3/launches?offset=${ offset }&limit=${ limit }`);
  }
}
