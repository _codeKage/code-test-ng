import { Injectable } from '@angular/core';

import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { SpaceXService } from './space-x.service';

@Injectable({
  providedIn: 'root'
})
export class LaunchStore {

    constructor(private spaceXService: SpaceXService) {}

    launches$: BehaviorSubject<any[]> = new BehaviorSubject([]);
    searchFilter: BehaviorSubject<string> = new BehaviorSubject('');

    init(): Observable<void> {
        return new Observable(observer => {
            this.spaceXService.getResults(10, 0).subscribe((launches) => {
                this.launches$.next(launches);
                observer.next(null);
                observer.complete();
            });
        });
    }

    lazyLoadLaunches(offset: number): Observable<any[]> {
        return this.spaceXService.getResults(10, offset).pipe(
            tap(launches => {
                const $launches = this.launches$.value;
                const newLaunches = [...$launches, ...launches];
                this.launches$.next(newLaunches);
            })
        );
    }

    getAll(): Observable<any[]> {
        return combineLatest([
            this.launches$,
            this.searchFilter
        ]).pipe(
            map(([launches, searchKey]) => {
                if (searchKey === '') {
                    return launches;
                }
                return launches.filter(launch => launch.mission_name.toLowerCase().includes(searchKey.toLowerCase()));
            })
        );
    }

    setFilter(key: string): void {
        this.searchFilter.next(key);
    }

}
