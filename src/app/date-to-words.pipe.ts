import { Pipe, PipeTransform } from '@angular/core';

import { formatDistanceToNow } from 'date-fns';

@Pipe({
  name: 'dateToWords'
})
export class DateToWordsPipe implements PipeTransform {

  transform(date: string): any {
    const formattedDate = new Date(date);
    return formatDistanceToNow(formattedDate, {
      addSuffix: true
    });
  }

}
