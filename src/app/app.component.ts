import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { LaunchStore } from './launch.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  isLoading: boolean;
  isLazyLoading: boolean;
  launches$: Observable<any[]>;
  searckKey: string;
  noMoreResult: boolean;

  constructor(private launchStore: LaunchStore) {
    this.searckKey = '';
  }

  ngOnInit(): void {
    this.init();
  }

  onScroll(launches: any[]): void {
    if (this.noMoreResult) {
      return;
    }
    if (launches.length % 10 === 0) {
      this.isLazyLoading = true;
      this.launchStore.lazyLoadLaunches(launches[launches.length - 1].flight_number).subscribe((launches$) => {
        this.isLazyLoading = false;
        if (launches$.length !== 10) {
          this.noMoreResult = true;
        }
      });
    }
  }

  onSearchInput(): void {
    this.launchStore.setFilter(this.searckKey);
  }

  private assignLaunches(): void {
    this.launches$ = this.launchStore.getAll();
  }

  private init(): void {
    this.isLoading = true;
    this.launchStore.init().subscribe(() => {
      this.assignLaunches();
      this.isLoading = false;
    });
  }

}

